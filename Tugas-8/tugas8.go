package main

import (
	"fmt"
	"strconv"
)

// SOAL 1
type segitigaSamaSisi struct{
  alas, tinggi int
}
// Method luas dan keliling segitigaSamaSisi
func (s segitigaSamaSisi) luas() int {
	return s.alas * s.tinggi / 2
}
func (s segitigaSamaSisi) keliling() int {
	return s.alas * 3
}

type persegiPanjang struct{
  panjang, lebar int
}
// Method luas dan keliling persegiPanjang
func (p persegiPanjang) luas() int {
	return p.panjang * p.lebar
}
func (p persegiPanjang) keliling() int {
	return 2*p.panjang + 2*p.lebar
}

type tabung struct{
  jariJari, tinggi float64
}
// Method volume dan luasPermukaan tabung
func (t tabung) volume() float64 {
	const phi = 3.14
	return phi * t.jariJari * t.jariJari * t.tinggi
}
func (t tabung) luasPermukaan() float64 {
	const phi = 3.14
	return 2 * phi * t.jariJari * (t.jariJari + t.tinggi)
}

type balok struct{
  panjang, lebar, tinggi int
}
// Method volume dan luasPermukaan balok
func (b balok) volume() float64 {
	return float64(b.panjang) * float64(b.lebar) * float64(b.tinggi)
}
func (b balok) luasPermukaan() float64 {
	return (2 * float64(b.panjang) * float64(b.lebar)) + (2 * float64(b.panjang) * float64(b.tinggi)) + (2 * float64(b.lebar) * float64(b.tinggi))
}

type hitungBangunDatar interface{
  luas() int
  keliling() int
}

type hitungBangunRuang interface{
  volume() float64
  luasPermukaan() float64
}

// SOAL 2
type phone struct{
   name, brand string
   year int
   colors []string
}
type tampilkanData interface {
	tambahWarna()
	tampilData()
}
func (hp *phone) tambahWarna(colors *[]string, warna ...string) {
	hp.colors = append(hp.colors, warna...)
}
func (hp phone) tampilData() {
	fmt.Println("name :", hp.name)
	fmt.Println("brand :", hp.brand)
	fmt.Println("year :", hp.year)
	var warna string
	batas := 0
	for _, item := range hp.colors {
		warna = warna + item
		batas++
		if batas < len(hp.colors) {
			warna += ", "
		}
	}
	fmt.Println("colors :", warna)
}

// cara trainer
type description interface {
	getDescription() string
}
func (ph phone) getDescription() string{
	var desc =  "name : "+ ph.name + "\n" +
		"brand : "+ ph.brand + "\n" +
		"year : "+ strconv.Itoa(ph.year) + "\n" +
		"name : "
		for index, item := range ph.colors{
			if index == 0{
				desc += item
			}else{
				desc += ", " + item
			}
		}
	return desc
}

// SOAL 3
func luasPersegi(sisi int, nilai bool) interface{} {
	luas := sisi*sisi
	
	if sisi == 0 {
		if nilai == true {
			return "Maaf anda belum menginput sisi dari persegi"
		} else {
			return nil
		}
	} else {
		if nilai == true {
			return "luas persegi dengan sisi " + strconv.Itoa(sisi) + " cm adalah " + strconv.Itoa(luas) + " cm"
		} else {
			return luas
		}
	}

	// cara trainer
	// switch {
	// case sisi > 0 && nilai:
	// 	return "luas persegi dengan sisi "+ strconv.Itoa(sisi) + " cm adalah " + strconv.Itoa(sisi*sisi) + " cm"
	// case sisi > 0 && !nilai:
	// 	return sisi * sisi
	// case sisi == 0 && nilai:
	// 	return "Maaf anda belum menginput sisi dari persegi"
	// default:
	// 	return nil
	// }
}
	
func main() {
	// SOAL 1
	fmt.Println("------Soal 1------")
	// Segitiga
	var segitiga hitungBangunDatar = segitigaSamaSisi{6,8}
	fmt.Println("Luas Segitiga :", segitiga.luas())
	fmt.Println("Keliling Segitiga :", segitiga.keliling())

	// PersegiPanjang
	var persegiPanjang hitungBangunDatar = persegiPanjang{6,8}
	fmt.Println("\nLuas Persegi Panjang :", persegiPanjang.luas())
	fmt.Println("Keliling Persegi Panjang :", persegiPanjang.keliling())

	// Tabung
	var tabung hitungBangunRuang = tabung{7,8}
	fmt.Println("\nVolume Tabung :", tabung.volume())
	fmt.Println("Luas Permukaan Tabung :", tabung.luasPermukaan())
	
	// Tabung
	var balok hitungBangunRuang = balok{7,8,9}
	fmt.Println("\nVolume Balok :", balok.volume())
	fmt.Println("Luas Permukaan Balok :", balok.luasPermukaan())

	// SOAL 2
	fmt.Println("------Soal 2------")
	var samsung = phone{name: "Samsung Galaxy Note 20", brand: "Samsung Galaxy Note 20", year: 2020}
	samsung.tambahWarna(&samsung.colors, "Mystic Bronze", "Mystic White", "Mystic Black")
	samsung.tampilData()

	// cara trainer
	var samsung1 description = phone{
		name: "Samsung Galaxy Note 20",
		brand: "Samsung",
		year: 2020,
		colors: []string{"Mystic Bronze", "Mystic White", "Mystic Black"},
	}

	fmt.Println(samsung1.getDescription())

	// SOAL 3
	fmt.Println("------Soal 3------")
	fmt.Println(luasPersegi(4, true))

	fmt.Println(luasPersegi(8, false))

	fmt.Println(luasPersegi(0, true))

	fmt.Println(luasPersegi(0, false))

	// SOAL 4
	fmt.Println("------Soal 4------")
	var prefix interface{}= "hasil penjumlahan dari "

	var kumpulanAngkaPertama interface{} = []int{6,8}

	var kumpulanAngkaKedua interface{} = []int{12,14}

	// tulis jawaban anda disini
	var kata = prefix.(string)
	angka1 := 0
	batas := 0
	for _, item := range kumpulanAngkaPertama.([]int) {
		angka1 += item
		kata = kata + strconv.Itoa(item) + " + "
	}
	for _, item := range kumpulanAngkaKedua.([]int) {
		angka1 += item
		kata = kata + strconv.Itoa(item)
		batas++
		if batas < len(kumpulanAngkaKedua.([]int)) {
			kata += " + "
		}
		
	}
	fmt.Println(kata, "=", angka1)
}