package main

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"strconv"
)

type NilaiMahasiswa struct {
	ID 			uint   `json:"id"`
	Nama 		string `json:"nama"`
	MataKuliah 	string `json:"matkul"`
	Nilai		uint   `json:"nilai"`
	IndeksNilai string `json:"indeks"`
}

var nilaiNilaiMahasiswa = []NilaiMahasiswa{}

func getIndeksNilai(nilai uint) string {
	switch {
	case nilai >= 80:
		return "A"
	case nilai >= 70 && nilai < 80:
		return "B"
	case nilai >= 60 && nilai < 70:
		return "C"
	case nilai >= 50 && nilai < 60:
		return "D"
	default:
		return "E"
	}
}

func GetNilaiMahasiswa(rw http.ResponseWriter, r *http.Request) {
	if r.Method == "GET" {
		dataNilaiMahasiswa, err := json.Marshal(nilaiNilaiMahasiswa)
		if err != nil {
			fmt.Println(err.Error())
			return
		}
		// pengecekan error versi trainer
		if err != nil {
			http.Error(rw, err.Error(), http.StatusInternalServerError)
		}

		rw.Header().Set("Content-Type", "application/json")
		rw.WriteHeader(http.StatusOK)
		rw.Write(dataNilaiMahasiswa)
		return
	}
	http.Error(rw, "method not allowed", http.StatusMethodNotAllowed)
}

func PostNilaiMahasiswa(rw http.ResponseWriter, r *http.Request) {
	rw.Header().Set("Content-Type", "application/json")
	var newMahasiswa = NilaiMahasiswa{ID: uint(len(nilaiNilaiMahasiswa)+1)}

	if r.Method == "POST" {
		if r.Header.Get("Content-Type") == "application/json"{
			// parse from json
			mahasiswaJson := json.NewDecoder(r.Body)
			if err := mahasiswaJson.Decode(&newMahasiswa); err != nil{
				log.Fatal(err)
			}
			if newMahasiswa.Nilai >= 80 {
				newMahasiswa.IndeksNilai = "A"
			}else if newMahasiswa.Nilai >= 70 && newMahasiswa.Nilai < 80 {
				newMahasiswa.IndeksNilai = "B"
			}else if newMahasiswa.Nilai >= 60 && newMahasiswa.Nilai < 70 {
				newMahasiswa.IndeksNilai = "C"
			}else if newMahasiswa.Nilai >= 50 && newMahasiswa.Nilai < 60 {
				newMahasiswa.IndeksNilai = "D"
			}else if newMahasiswa.Nilai < 50 {
				newMahasiswa.IndeksNilai = "E"
			}
			// if newMahasiswa.Nilai <= 100 {
			// }else{
			// 	newMahasiswa.Nilai = 0
			// 	rw.Write([]byte("Nilai Tidak Boleh lebih dari 100!!\n"))
			// }
		}else{
			// parse from form data
			nama := r.PostFormValue("nama")
			fmt.Println(nama)
			matkul := r.PostFormValue("matkul")
			fmt.Println(matkul)
			newMahasiswa.Nama = nama
			newMahasiswa.MataKuliah = matkul
			getNilai := r.PostFormValue("nilai")
			nilai, _ := strconv.Atoi(getNilai)
			
			newMahasiswa.Nilai = uint(nilai)
			fmt.Println(getNilai)

			if newMahasiswa.Nilai >= 80 {
				newMahasiswa.IndeksNilai = "A"
			}else if newMahasiswa.Nilai >= 70 && newMahasiswa.Nilai < 80 {
				newMahasiswa.IndeksNilai = "B"
			}else if newMahasiswa.Nilai >= 60 && newMahasiswa.Nilai < 70 {
				newMahasiswa.IndeksNilai = "C"
			}else if newMahasiswa.Nilai >= 50 && newMahasiswa.Nilai < 60 {
				newMahasiswa.IndeksNilai = "D"
			}else if newMahasiswa.Nilai < 50 {
				newMahasiswa.IndeksNilai = "E"
			}
			// if nilai <= 100 {
			// }else{
			// 	rw.Write([]byte("Nilai Tidak Boleh lebih dari 100!!\n"))
			// 	// rw.Write([]byte("\n"))
			// }
		}

		validation := ""

		if newMahasiswa.Nilai > 100 {
			if len(validation) == 0 {
				validation = "Nilai tidak boleh lebih dari 100"
			} else {
				validation += " dan Nilai tidak boleh lebih dari 100"
			}
		}

		if len(validation) > 0 {
			http.Error(rw, validation, http.StatusBadRequest)
			return
		}

		// pengecekan nilai lebih dari 100 by trainer
		if newMahasiswa.Nilai > 100 {
			http.Error(rw, "Nilai tidak boleh diinput lebih dari 100", http.StatusBadRequest)
		}

		// function getIndeksNilai by trainer
		newMahasiswa.IndeksNilai = getIndeksNilai(newMahasiswa.Nilai)

		nilaiNilaiMahasiswa = append(nilaiNilaiMahasiswa, newMahasiswa)
		dataNilaiMahasiswa, _ := json.Marshal(newMahasiswa)
		rw.Write(dataNilaiMahasiswa)
		return
	}
	http.Error(rw, "method not allowed", http.StatusMethodNotAllowed)
}

func Auth(next http.Handler) http.Handler {
	return http.HandlerFunc(func(rw http.ResponseWriter, r *http.Request) {
		username, password, ok := r.BasicAuth()
		// tidak memasukkan username dan password
		if !ok {
			rw.Write([]byte("Username dan Password tidak boleh kosong"))
			return
		}
		// memasukkan username dan password
		if username == "abiyyu" && password =="admin" {
			next.ServeHTTP(rw, r)
			return
		}
		// memasukkan usernama atau password yang salah
		rw.Write([]byte("Username atau Password tidak sesuai"))
		return
	})
}

func main() {
	http.HandleFunc("/nilai-mahasiswa", GetNilaiMahasiswa)
	http.Handle("/post-nilai-mahasiswa",Auth(http.HandlerFunc(PostNilaiMahasiswa)))

	fmt.Println("Server is running at port 8080")

	log.Fatal(http.ListenAndServe("localhost:8080", nil))
}