package main

import (
	"fmt"
	"log"
	"net/http"
	"quiz-3/functions"

	"github.com/julienschmidt/httprouter"
)

func main() {
	// SOAL 1
		// Membuat folder Quiz-3 dan install driver mysql dan httprouter
		
	router := httprouter.New()

	// SOAL 2
	router.GET("/segitiga-sama-sisi", functions.SegitigaSamaSisi)
	router.GET("/persegi", functions.Persegi)
	router.GET("/persegi-panjang", functions.PersegiPanjang)
	router.GET("/lingkaran", functions.Lingkaran)
	router.GET("/jajar-genjang", functions.JajarGenjang)

	// SOAL 3 dan 5
	fmt.Println("------Soal 3------")
	router.GET("/categories", functions.GetCategory)
	router.POST("/categories", functions.Auth(functions.PostCategory))
	router.PUT("/categories/:id", functions.Auth(functions.UpdateCategory))
	router.DELETE("/categories/:id", functions.Auth(functions.DeleteCategory))
	router.GET("/categories/:id/books", functions.GetBooksByCategoryFunc)

	router.GET("/books", functions.GetBookFunc)
	router.POST("/books", functions.Auth(functions.PostBookFunc))
	router.PUT("/books/:id", functions.Auth(functions.UpdateBookFunc))
	router.DELETE("/books/:id", functions.Auth(functions.DeleteBookFunc))

	fmt.Println("Server Running at Port 8080")
	log.Fatal(http.ListenAndServe("localhost:8080", router))
}