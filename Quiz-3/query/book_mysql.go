package query

import (
	"context"
	"database/sql"
	"errors"
	"fmt"
	"log"
	"quiz-3/config"
	"quiz-3/models"
	"time"
)
const (
	book_table = "book"
)

//GetAll Book
func GetAllBook(ctx context.Context)([]models.Book, error) {
	var Book []models.Book
	db, err := config.MySQL()

	// config db check
	if err != nil {
		log.Fatal("Can't Connect to MySQL", err)
	}

	// query
	queryText := fmt.Sprintf("SELECT * FROM %v Order By created_at DESC", book_table)
	rowQuery, err := db.QueryContext(ctx, queryText)

	if err != nil {
		log.Fatal(err)
	}

	for rowQuery.Next() {
    var book models.Book
    var createdAt, updatedAt string
    if err = rowQuery.Scan(&book.ID,
		&book.Title,
		&book.Description,
		&book.ImageURL,
		&book.ReleaseYear,
		&book.Price,
		&book.TotalPage,
		&book.Thickness,
		&createdAt,
		&updatedAt,
		&book.Category); err != nil {
			return nil, err
    }

    //  Change format string to datetime for created_at and updated_at
    book.CreatedAt, err = time.Parse(layoutDateTime, createdAt)

    if err != nil {
      log.Fatal(err)
    }

    book.UpdatedAt, err = time.Parse(layoutDateTime, updatedAt)

    if err != nil {
      log.Fatal(err)
    }

    Book = append(Book, book)
  }
  return Book, nil
}

// Insert Book
func InsertBook(ctx context.Context, book models.Book) error {
	db, err := config.MySQL()

	// config db check
	if err != nil {
		log.Fatal("Can't Connect to MySQL", err)
	}
  
	// query
	queryText := fmt.Sprintf("INSERT INTO %v (title, description, image_url, release_year, price, total_page, thickness, created_at, updated_at, category_id) values('%v','%v','%v',%v,'%v',%v,'%v', NOW(), NOW(), %v)",
	book_table,
	book.Title,
    book.Description,
    book.ImageURL,
    book.ReleaseYear,
    book.Price,
    book.TotalPage,
    book.Thickness,
	book.Category)
	_, err = db.ExecContext(ctx, queryText)

	if err != nil {
		return err
	}
	return nil
}

// Update Book
func UpdateBook(ctx context.Context, book models.Book, idBook string) error {
	db, err := config.MySQL()
	if err != nil {
		log.Fatal("Can't Connect to MySQL", err)
	}

	queryText := fmt.Sprintf("UPDATE %v set title = '%v', description ='%v', image_url ='%v', release_year = %v, price = '%v', total_page = %v, thickness = '%v', updated_at = NOW(), category_id = %v where id = %v",
    book_table,
    book.Title,
    book.Description,
    book.ImageURL,
    book.ReleaseYear,
    book.Price,
    book.TotalPage,
    book.Thickness,
	book.Category,
	idBook)

	_, err = db.ExecContext(ctx, queryText)
	if err != nil {
    	return err
	}

	return nil
}

// Delete Book
func DeleteBook(ctx context.Context, idBook string) error {
    db, err := config.MySQL()
    if err != nil {
        log.Fatal("Can't Connect to MySQL", err)
    }

    queryText := fmt.Sprintf("DELETE FROM %v where id = %v", book_table, idBook)

    s, err := db.ExecContext(ctx, queryText)

    if err != nil && err != sql.ErrNoRows {
        return err
    }

    check, err := s.RowsAffected()
    fmt.Println(check)
    if check == 0 {
        return errors.New("id tidak ada")
    }

    if err != nil {
        fmt.Println(err.Error())
    }

    return nil
}