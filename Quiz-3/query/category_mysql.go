package query

import (
	"context"
	"database/sql"
	"errors"
	"fmt"
	"log"
	"quiz-3/config"
	"quiz-3/models"
	"time"
)
const (
	category_table = "category"
	layoutDateTime  = "2006-01-02 15:04:05"
)

//Get All Category 
func GetAllCategory(ctx context.Context)([]models.Category, error) {
	var Category []models.Category
	db, err := config.MySQL()

	// config db check
	if err != nil {
		log.Fatal("Can't Connect to MySQL", err)
	}

	// query
	queryText := fmt.Sprintf("SELECT * FROM %v Order By created_at DESC", category_table)
	rowQuery, err := db.QueryContext(ctx, queryText)

	if err != nil {
		log.Fatal(err)
	}

	for rowQuery.Next() {
    var category models.Category
    var createdAt, updatedAt string
    if err = rowQuery.Scan(&category.ID,
      &category.Name,
      &createdAt,
      &updatedAt); err != nil {
      return nil, err
    }

    //  Change format string to datetime for created_at and updated_at
    category.CreatedAt, err = time.Parse(layoutDateTime, createdAt)

    if err != nil {
      log.Fatal(err)
    }

    category.UpdatedAt, err = time.Parse(layoutDateTime, updatedAt)

    if err != nil {
      log.Fatal(err)
    }

    Category = append(Category, category)
  }
  return Category, nil
}

// Insert Category
func InsertCategory(ctx context.Context, category models.Category) error {
  db, err := config.MySQL()

  // config db check
  if err != nil {
    log.Fatal("Can't Connect to MySQL", err)
  }
  
  // query
  queryText := fmt.Sprintf("INSERT INTO %v (name, created_at, updated_at) values('%v', NOW(), NOW())", category_table,
    category.Name)
  _, err = db.ExecContext(ctx, queryText)

  if err != nil {
    return err
  }
  return nil
}

// Update Category
func UpdateCategory(ctx context.Context, category models.Category, idCategory string) error {
  db, err := config.MySQL()
  if err != nil {
    log.Fatal("Can't Connect to MySQL", err)
  }

  queryText := fmt.Sprintf("UPDATE %v set name ='%v', updated_at = NOW() where id = %v",
    category_table,
    category.Name,
    idCategory,
  )

  _, err = db.ExecContext(ctx, queryText)
  if err != nil {
    return err
  }

  return nil
}

// Delete Category
func DeleteCategory(ctx context.Context, idCategory string) error {
    db, err := config.MySQL()
    if err != nil {
        log.Fatal("Can't Connect to MySQL", err)
    }

    queryText := fmt.Sprintf("DELETE FROM %v where id = %s", category_table, idCategory)

    s, err := db.ExecContext(ctx, queryText)

    if err != nil && err != sql.ErrNoRows {
        return err
    }

    check, err := s.RowsAffected()
    fmt.Println(check)
    if check == 0 {
        return errors.New("id tidak ada")
    }

    if err != nil {
        fmt.Println(err.Error())
    }

    return nil
}

//Get All Books by Category
func GetAllBooksByCategory(ctx context.Context, idBooksByCategory string)([]models.Book, error) {
	var BooksByCategory []models.Book
	db, err := config.MySQL()

	// config db check
	if err != nil {
		log.Fatal("Can't Connect to MySQL", err)
	}

	// query
	queryText := fmt.Sprintf("SELECT * FROM %v where category_id in (select id from %v where id = %v)", book_table, category_table, idBooksByCategory)
	rowQuery, err := db.QueryContext(ctx, queryText)

	if err != nil {
		log.Fatal(err)
	}

	for rowQuery.Next() {
    var booksByCategory models.Book
    var createdAt, updatedAt string
    if err = rowQuery.Scan(&booksByCategory.ID,
		&booksByCategory.Title,
		&booksByCategory.Description,
		&booksByCategory.ImageURL,
		&booksByCategory.ReleaseYear,
		&booksByCategory.Price,
		&booksByCategory.TotalPage,
		&booksByCategory.Thickness,
		&createdAt,
		&updatedAt,
		&booksByCategory.Category); err != nil {
			return nil, err
    }

    //  Change format string to datetime for created_at and updated_at
    booksByCategory.CreatedAt, err = time.Parse(layoutDateTime, createdAt)

    if err != nil {
      log.Fatal(err)
    }

    booksByCategory.UpdatedAt, err = time.Parse(layoutDateTime, updatedAt)

    if err != nil {
      log.Fatal(err)
    }

    BooksByCategory = append(BooksByCategory, booksByCategory)
  }
  return BooksByCategory, nil
}