package functions

import (
	"context"
	"encoding/json"
	"fmt"
	"net/http"
	"net/url"
	"quiz-3/models"
	"quiz-3/query"
	"quiz-3/utils"

	"github.com/julienschmidt/httprouter"
)

// Function Thickness
func getThickness(total_page int) string {
	switch {
	case total_page <= 100:
		return "Tipis"
	case total_page > 100 && total_page <= 200:
		return "Sedang"
	default:
		return "Tebal"
	}
}

// Function Check Input URL
func IsUrl(str string) bool {
    u, err := url.Parse(str)
    return err == nil && u.Scheme != "" && u.Host != ""
}

// Get Book
func GetBookFunc(rw http.ResponseWriter, _ *http.Request, _ httprouter.Params) {
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()
	bookFunc, err := query.GetAllBook(ctx)

  	if err != nil {
    	fmt.Println(err)
  	}

  	utils.ResponseJSON(rw, bookFunc, http.StatusOK)
}

// Post Book
func PostBookFunc(rw http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	// pengecekan inputannya JSON atau bukan
	if r.Header.Get("Content-Type") != "application/json" {
		http.Error(rw, "Gunakan content type application / json", http.StatusBadRequest)
		return
	}
	
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()
	var bookFunc models.Book
	if err := json.NewDecoder(r.Body).Decode(&bookFunc); err != nil {
		utils.ResponseJSON(rw, err, http.StatusBadRequest)
		return
	}

	validation := ""

	if !IsUrl(bookFunc.ImageURL) {
		if len(validation) == 0 {
			validation = "image_url hanya bisa diisi url"
		} else {
			validation += " dan image_url hanya bisa diisi url"
		}
	}
	if bookFunc.ReleaseYear <=1980 && bookFunc.ReleaseYear >=2021 {
		if len(validation) == 0 {
			validation = "Tahun Rilis minimal tahun 1980 dan maksimal tahun 2021"
		} else {
			validation += " dan Tahun Rilis minimal tahun 1980 dan maksimal tahun 2021"
		}
	}
	if len(validation) > 0 {
		http.Error(rw, validation, http.StatusBadRequest)
		return
	}

	bookFunc.Thickness = getThickness(bookFunc.TotalPage)
	
	if err := query.InsertBook(ctx, bookFunc); err != nil {
		utils.ResponseJSON(rw, err, http.StatusInternalServerError)
		return
	}
	
	res := map[string]string{
		"status": "Succesfully",
	}
	
	utils.ResponseJSON(rw, res, http.StatusCreated)
}

// Update Book
func UpdateBookFunc(rw http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	if r.Header.Get("Content-Type") != "application/json" {
		http.Error(rw, "Gunakan content type application / json", http.StatusBadRequest)
		return
	}
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()
	var bookFunc models.Book
	
	if err := json.NewDecoder(r.Body).Decode(&bookFunc); err != nil {
		utils.ResponseJSON(rw, err, http.StatusBadRequest)
		return
	}
	
	var idBook = ps.ByName("id")
	
	validation := ""

	if !IsUrl(bookFunc.ImageURL) {
		if len(validation) == 0 {
			validation = "image_url hanya bisa diisi url"
		} else {
			validation += " dan image_url hanya bisa diisi url"
		}
	}
	if bookFunc.ReleaseYear < 1980 || bookFunc.ReleaseYear > 2021{
		if len(validation) == 0 {
			validation = "Tahun Rilis minimal tahun 1980 dan maksimal tahun 2021"
		} else {
			validation += " dan Tahun Rilis minimal tahun 1980 dan maksimal tahun 2021"
		}
	}
	if len(validation) > 0 {
		http.Error(rw, validation, http.StatusBadRequest)
		return
	}

	bookFunc.Thickness = getThickness(bookFunc.TotalPage)
	
	if err := query.UpdateBook(ctx, bookFunc, idBook); err != nil {
		utils.ResponseJSON(rw, err, http.StatusInternalServerError)
		return
	}
	
	res := map[string]string{
		"status": "Succesfully",
	}

	utils.ResponseJSON(rw, res, http.StatusCreated)
}

// Delete Book
func DeleteBookFunc(rw http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()
	var idBook = ps.ByName("id")

	if err := query.DeleteBook(ctx, idBook); err != nil {
		kesalahan := map[string]string{
			"error": fmt.Sprintf("%v", err),
		}
		utils.ResponseJSON(rw, kesalahan, http.StatusInternalServerError)
		return
	}
	
	res := map[string]string{
		"status": "Succesfully",
	}
	utils.ResponseJSON(rw, res, http.StatusOK)
}