package functions

import (
	"fmt"
	"math"
	"net/http"
	"strconv"

	"github.com/julienschmidt/httprouter"
)

var LuasSegitigaSamaSisiCh = make(chan float64)
var KelilingSegitigaSamaSisiCh = make(chan float64)
var LuasPersegiCh = make(chan int)
var KelilingPersegiCh = make(chan int)
var LuasPersegiPanjangCh = make(chan int)
var KelilingPersegiPanjangCh = make(chan int)
var LuasLingkaranCh = make(chan float64)
var KelilingLingkaranCh = make(chan float64)
var LuasJajarGenjangCh = make(chan int)
var KelilingJajarGenjangCh = make(chan int)

func SegitigaSamaSisi(rw http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	getAlas := r.URL.Query().Get("alas")
	alas, _ := strconv.Atoi(getAlas)
	getTinggi := r.URL.Query().Get("tinggi")
	tinggi, _ := strconv.Atoi(getTinggi)
	hitung := r.URL.Query().Get("hitung")

	go hitungSegitigaSamaSisi(float64(alas), float64(tinggi), hitung)
	// go hitungKelilingSegitigaSamaSisi(7, KelilingSegitigaSamaSisiCh)
	fmt.Fprintf(rw, "Luas Segitiga Sama Sisi dengan Alas %d cm dan Tinggi %d cm adalah %d cm\n",alas, tinggi, int(<-LuasSegitigaSamaSisiCh))
	fmt.Fprintf(rw, "Keliling Segitiga Sama Sisi dengan Alas %d cm dan Tinggi %d cm adalah %d cm",alas, tinggi, int(<-KelilingSegitigaSamaSisiCh))
}
func hitungSegitigaSamaSisi(alas, tinggi float64, hitung string) {
	if hitung == "luas" {
		LuasSegitigaSamaSisiCh <- math.Round(alas * tinggi / 2)
	}else if hitung == "kelilling" {
		KelilingSegitigaSamaSisiCh <- math.Round(alas * 3)
	}
}
// func hitungKelilingSegitigaSamaSisi(alas float64, hitung chan float64) {
// 	hitung <- math.Round(alas * 3)
// }

func Persegi(rw http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	go hitungLuasPersegi(7, LuasPersegiCh)
	go hitungKelilingPersegi(7, KelilingPersegiCh)
	fmt.Fprintf(rw, "Luas Persegi dengan Sisi 7 cm adalah %d cm\n", int(<-LuasPersegiCh))
	fmt.Fprintf(rw, "Keliling Persegi dengan Sisi 7 cm adalah %d cm", int(<-KelilingPersegiCh))
}
func hitungLuasPersegi(sisi int, hitung chan int) {
	hitung <- sisi * sisi
}
func hitungKelilingPersegi(sisi int, hitung chan int) {
	hitung <- sisi * 4
}

func PersegiPanjang(rw http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	go hitungLuasPersegiPanjang(7, 10, LuasPersegiPanjangCh)
	go hitungKelilingPersegiPanjang(7, 10, KelilingPersegiPanjangCh)
	fmt.Fprintf(rw, "Luas Persegi Panjang dengan Panjang 7 cm dan Lebar 10 cm adalah %d cm\n", <-LuasPersegiPanjangCh)
	fmt.Fprintf(rw, "Keliling Persegi Panjang dengan Panjang 7 cm dan Lebar 10 cm adalah %d cm", <-KelilingPersegiPanjangCh)
}
func hitungLuasPersegiPanjang(panjang, lebar int, hitung chan int) {
	hitung <- panjang * lebar
}
func hitungKelilingPersegiPanjang(panjang, lebar int, hitung chan int) {
	hitung <- 2*panjang + 2*lebar
}

func Lingkaran(rw http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	go hitungLuasLingkaran(7, LuasLingkaranCh)
	go hitungKelilingLingkaran(7, KelilingLingkaranCh)
	fmt.Fprintf(rw, "Luas Lingkaran dengan Jari-Jari 7 cm adalah %d cm\n", int(<-LuasLingkaranCh))
	fmt.Fprintf(rw, "Keliling Lingkaran dengan Jari-Jari 7 cm adalah %d cm", int(<-KelilingLingkaranCh))
}
func hitungLuasLingkaran(jarijari float64, hitung chan float64) {
	hitung <- math.Round(math.Pi * jarijari * jarijari)
}
func hitungKelilingLingkaran(jarijari float64, hitung chan float64) {
	hitung <- math.Round(math.Pi * jarijari * 2)
}

func JajarGenjang(rw http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	go hitungLuasJajarGenjang(10, 14, LuasJajarGenjangCh)
	go hitungKelilingJajarGenjang(10, 7, KelilingJajarGenjangCh)
	fmt.Fprintf(rw, "Luas Jajar Genjang dengan Sisi 7 cm, Alas 10 cm, dan Tinggi 14 cm adalah %d cm\n", <-LuasJajarGenjangCh)
	fmt.Fprintf(rw, "Keliling Jajar Genjang dengan Sisi 7 cm, Alas 10 cm, dan Tinggi 14 cm adalah %d cm", <-KelilingJajarGenjangCh)
}
func hitungLuasJajarGenjang(alas, tinggi int, hitung chan int) {
	hitung <- alas * tinggi
}
func hitungKelilingJajarGenjang(alas, sisi int, hitung chan int) {
	hitung <- 2*alas + 2*sisi
}

// func main() {
// 	// SOAL 2
// 	fmt.Println("------Soal 2------")
// 	http.HandleFunc("/", func(rw http.ResponseWriter, r *http.Request) {
// 		fmt.Fprintln(rw, "THIS IS ROOT OR HOME PAGE")
// 	})

// 	http.HandleFunc("/segitiga-sama-sisi", SegitigaSamaSisi)
// 	http.HandleFunc("/persegi", Persegi)
// 	http.HandleFunc("/persegi-panjang", PersegiPanjang)
// 	http.HandleFunc("/lingkaran", Lingkaran)
// 	http.HandleFunc("/jajar-genjang", JajarGenjang)

// 	fmt.Println("server is running at port 8080")

// 	http.ListenAndServe("localhost:8080", nil)
// }