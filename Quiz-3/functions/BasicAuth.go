package functions

import (
	"net/http"

	"github.com/julienschmidt/httprouter"
)

// SOAL 5
func Auth(next httprouter.Handle) httprouter.Handle {
	return func(rw http.ResponseWriter, r *http.Request, ps httprouter.Params) {
		username, password, ok := r.BasicAuth()
		// tidak memasukkan username dan password
		if !ok {
			rw.Write([]byte("Username dan Password tidak boleh kosong"))
			return
		}
		// memasukkan username dan password
		if username == "admin" && password == "password" {
			next(rw, r, ps)
			return
		} else if username == "editor" && password == "secret" {
			next(rw, r, ps)
			return
		} else if username == "trainer" && password == "rahasia" {
			next(rw, r, ps)
			return
		}

		// memasukkan usernama atau password yang salah
		rw.Write([]byte("Username atau Password tidak sesuai"))
	}
}