package functions

import (
	"context"
	"encoding/json"
	"fmt"
	"net/http"
	"quiz-3/models"
	"quiz-3/query"
	"quiz-3/utils"

	"github.com/julienschmidt/httprouter"
)

// Get Category
func GetCategory(rw http.ResponseWriter, _ *http.Request, _ httprouter.Params) {
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()
	category, err := query.GetAllCategory(ctx)

  	if err != nil {
    	fmt.Println(err)
  	}

  	utils.ResponseJSON(rw, category, http.StatusOK)
}

// Post Category
func PostCategory(rw http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	// pengecekan inputannya JSON atau bukan
	if r.Header.Get("Content-Type") != "application/json" {
		http.Error(rw, "Gunakan content type application / json", http.StatusBadRequest)
		return
	}
	
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()
	var category models.Category
	if err := json.NewDecoder(r.Body).Decode(&category); err != nil {
		utils.ResponseJSON(rw, err, http.StatusBadRequest)
		return
	}
	
	if err := query.InsertCategory(ctx, category); err != nil {
		utils.ResponseJSON(rw, err, http.StatusInternalServerError)
		return
	}
	
	res := map[string]string{
		"status": "Succesfully",
	}
	
	utils.ResponseJSON(rw, res, http.StatusCreated)
}

// Update Category
func UpdateCategory(rw http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	if r.Header.Get("Content-Type") != "application/json" {
		http.Error(rw, "Gunakan content type application / json", http.StatusBadRequest)
		return
	}
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()
	var category models.Category
	
	if err := json.NewDecoder(r.Body).Decode(&category); err != nil {
		utils.ResponseJSON(rw, err, http.StatusBadRequest)
		return
	}
	
	var idCategory = ps.ByName("id")
	
	if err := query.UpdateCategory(ctx, category, idCategory); err != nil {
		utils.ResponseJSON(rw, err, http.StatusInternalServerError)
		return
	}
	
	res := map[string]string{
		"status": "Succesfully",
	}

	utils.ResponseJSON(rw, res, http.StatusCreated)
}

// Delete Category
func DeleteCategory(rw http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()
	var idCategory = ps.ByName("id")

	if err := query.DeleteCategory(ctx, idCategory); err != nil {
		kesalahan := map[string]string{
			"error": fmt.Sprintf("%v", err),
		}
		utils.ResponseJSON(rw, kesalahan, http.StatusInternalServerError)
		return
	}
	
	res := map[string]string{
		"status": "Succesfully",
	}
	utils.ResponseJSON(rw, res, http.StatusOK)
}

// Get Books By Category
func GetBooksByCategoryFunc(rw http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	var idBooksByCategory = ps.ByName("id")

	booksByCategoryFunc, err := query.GetAllBooksByCategory(ctx, idBooksByCategory)

  	if err != nil {
    	fmt.Println(err)
  	}

  	utils.ResponseJSON(rw, booksByCategoryFunc, http.StatusOK)
}
