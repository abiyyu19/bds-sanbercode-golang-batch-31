package main

import (
	"fmt"
	"strconv"
	"strings"
)

func main() {

	// SOAL 1
	var kata1 = "Bootcamp "
	kata2 := "Digital "
	var kata3 = "Skill "
	kata4 := "Sanbercode "
	var kata5 = "Golang"

	var gabung = kata1 + kata2 + kata3 + kata4 + kata5

	fmt.Println(gabung)

	//SOAL 2
	halo := "Halo Dunia"
	var newhalo = strings.Replace(halo, "Dunia", "Golang", 1)
	fmt.Println(newhalo)

	//SOAL 3
	var kataPertama = "saya"
	var kataKedua = "senang"
	var kataKetiga = "belajar"
	var kataKeempat = "golang"

	fmt.Println(kataPertama + " " + strings.Title(kataKedua) + " " + kataKetiga[:6] + strings.ToUpper(kataKetiga[6:]) + " " + strings.ToUpper(kataKeempat))

	//SOAL 4
	var angkaPertama = "8"
	var angkaKedua = "5"
	var angkaKetiga = "6"
	var angkaKeempat = "7"

	var angka1, _ = strconv.Atoi(angkaPertama)
	var angka2, _ = strconv.Atoi(angkaKedua)
	var angka3, _ = strconv.Atoi(angkaKetiga)
	var angka4, _ = strconv.Atoi(angkaKeempat)

	fmt.Println(angka1 + angka2 + angka3 + angka4)

	//SOAL 5
	kalimat := "halo halo bandung"
	angka := 2021

	kalimat = strings.Replace(kalimat, "halo", "Hi", -1)

	var kalimat2 = `"` + kalimat + `" - `

	fmt.Printf("%s%d", kalimat2, angka)

}
