package models

import (
	"time"
)

type Nilai struct {
	ID          int       `json:"id"`
	Skor		int		  `json:"skor"`
	IndeksNilai string    `json:"indeks"`
   CreatedAt    time.Time `json:"created_at"`
   UpdatedAt    time.Time `json:"updated_at"`
    Mahasiswa	int		  `json:"mahasiswa_id"`
	MataKuliah  int		  `json:"mata_kuliah_id"`
}