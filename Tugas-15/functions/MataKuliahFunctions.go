package functions

import (
	"context"
	"encoding/json"
	"fmt"
	"net/http"
	"tugas15/models"
	"tugas15/query"
	"tugas15/utils"

	"github.com/julienschmidt/httprouter"
)

// Get MataKuliah
func GetMataKuliah(rw http.ResponseWriter, _ *http.Request, _ httprouter.Params) {
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()
	matkul, err := query.GetAllMataKuliah(ctx)

  	if err != nil {
    	fmt.Println(err)
  	}

  	utils.ResponseJSON(rw, matkul, http.StatusOK)
}

// Post MataKuliah
func PostMataKuliah(rw http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	// pengecekan inputannya JSON atau bukan
	if r.Header.Get("Content-Type") != "application/json" {
		http.Error(rw, "Gunakan content type application / json", http.StatusBadRequest)
		return
	}
	
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()
	var matkul models.MataKuliah
	if err := json.NewDecoder(r.Body).Decode(&matkul); err != nil {
		utils.ResponseJSON(rw, err, http.StatusBadRequest)
		return
	}
	
	if err := query.InsertMataKuliah(ctx, matkul); err != nil {
		utils.ResponseJSON(rw, err, http.StatusInternalServerError)
		return
	}
	
	res := map[string]string{
		"status": "Succesfully",
	}
	
	utils.ResponseJSON(rw, res, http.StatusCreated)
}

// Update MataKuliah
func UpdateMataKuliah(rw http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	if r.Header.Get("Content-Type") != "application/json" {
		http.Error(rw, "Gunakan content type application / json", http.StatusBadRequest)
		return
	}
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()
	var matkul models.MataKuliah
	
	if err := json.NewDecoder(r.Body).Decode(&matkul); err != nil {
		utils.ResponseJSON(rw, err, http.StatusBadRequest)
		return
	}
	
	var idMatkul = ps.ByName("id")
	
	if err := query.UpdateMataKuliah(ctx, matkul, idMatkul); err != nil {
		utils.ResponseJSON(rw, err, http.StatusInternalServerError)
		return
	}
	
	res := map[string]string{
		"status": "Succesfully",
	}

	utils.ResponseJSON(rw, res, http.StatusCreated)
}

// Delete MataKuliah
func DeleteMataKuliah(rw http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()
	var idMatkul = ps.ByName("id")

	if err := query.DeleteMataKuliah(ctx, idMatkul); err != nil {
		kesalahan := map[string]string{
			"error": fmt.Sprintf("%v", err),
		}
		utils.ResponseJSON(rw, kesalahan, http.StatusInternalServerError)
		return
	}
	
	res := map[string]string{
		"status": "Succesfully",
	}
	utils.ResponseJSON(rw, res, http.StatusOK)
}