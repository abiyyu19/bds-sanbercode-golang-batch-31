package functions

import (
	"context"
	"encoding/json"
	"fmt"
	"net/http"
	"tugas15/models"
	"tugas15/query"
	"tugas15/utils"

	"github.com/julienschmidt/httprouter"
)

// Get Mahasiswa
func GetMahasiswa(rw http.ResponseWriter, _ *http.Request, _ httprouter.Params) {
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()
	mahasiswa, err := query.GetAllMahasiswa(ctx)

  	if err != nil {
    	fmt.Println(err)
  	}

  	utils.ResponseJSON(rw, mahasiswa, http.StatusOK)
}

// Post Mahasiswa
func PostMahasiswa(rw http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	// pengecekan inputannya JSON atau bukan
	if r.Header.Get("Content-Type") != "application/json" {
		http.Error(rw, "Gunakan content type application / json", http.StatusBadRequest)
		return
	}
	
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()
	var mahasiswa models.Mahasiswa
	if err := json.NewDecoder(r.Body).Decode(&mahasiswa); err != nil {
		utils.ResponseJSON(rw, err, http.StatusBadRequest)
		return
	}
	
	if err := query.InsertMahasiswa(ctx, mahasiswa); err != nil {
		utils.ResponseJSON(rw, err, http.StatusInternalServerError)
		return
	}
	
	res := map[string]string{
		"status": "Succesfully",
	}
	
	utils.ResponseJSON(rw, res, http.StatusCreated)
}

// Update Mahasiswa
func UpdateMahasiswa(rw http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	if r.Header.Get("Content-Type") != "application/json" {
		http.Error(rw, "Gunakan content type application / json", http.StatusBadRequest)
		return
	}
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()
	var mahasiswa models.Mahasiswa
	
	if err := json.NewDecoder(r.Body).Decode(&mahasiswa); err != nil {
		utils.ResponseJSON(rw, err, http.StatusBadRequest)
		return
	}
	
	var idMhs = ps.ByName("id")
	
	if err := query.UpdateMahasiswa(ctx, mahasiswa, idMhs); err != nil {
		utils.ResponseJSON(rw, err, http.StatusInternalServerError)
		return
	}
	
	res := map[string]string{
		"status": "Succesfully",
	}

	utils.ResponseJSON(rw, res, http.StatusCreated)
}

// Delete Mahasiswa
func DeleteMahasiswa(rw http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()
	var idMhs = ps.ByName("id")

	if err := query.DeleteMahasiswa(ctx, idMhs); err != nil {
		kesalahan := map[string]string{
			"error": fmt.Sprintf("%v", err),
		}
		utils.ResponseJSON(rw, kesalahan, http.StatusInternalServerError)
		return
	}
	
	res := map[string]string{
		"status": "Succesfully",
	}
	utils.ResponseJSON(rw, res, http.StatusOK)
}