package query

import (
	"context"
	"database/sql"
	"errors"
	"fmt"
	"log"
	"time"
	"tugas15/config"
	"tugas15/models"
)
const (
	mata_kuliah_table = "mata_kuliah"
)

//GetAll
func GetAllMataKuliah(ctx context.Context)([]models.MataKuliah, error) {
	var MataKuliah []models.MataKuliah
	db, err := config.MySQL()

	// config db check
	if err != nil {
		log.Fatal("Can't Connect to MySQL", err)
	}

	// query
	queryText := fmt.Sprintf("SELECT * FROM %v Order By created_at DESC", mata_kuliah_table)
	rowQuery, err := db.QueryContext(ctx, queryText)

	if err != nil {
		log.Fatal(err)
	}

	for rowQuery.Next() {
    var matkul models.MataKuliah
    var createdAt, updatedAt string
    if err = rowQuery.Scan(&matkul.ID,
      &matkul.Nama,
      &createdAt,
      &updatedAt); err != nil {
      return nil, err
    }

    //  Change format string to datetime for created_at and updated_at
    matkul.CreatedAt, err = time.Parse(layoutDateTime, createdAt)

    if err != nil {
      log.Fatal(err)
    }

    matkul.UpdatedAt, err = time.Parse(layoutDateTime, updatedAt)

    if err != nil {
      log.Fatal(err)
    }

    MataKuliah = append(MataKuliah, matkul)
  }
  return MataKuliah, nil
}

// Insert Mahasiswa
func InsertMataKuliah(ctx context.Context, matkul models.MataKuliah) error {
  db, err := config.MySQL()

  // config db check
  if err != nil {
    log.Fatal("Can't Connect to MySQL", err)
  }
  
  // query
  queryText := fmt.Sprintf("INSERT INTO %v (nama, created_at, updated_at) values('%v', NOW(), NOW())", mata_kuliah_table,
    matkul.Nama)
  _, err = db.ExecContext(ctx, queryText)

  if err != nil {
    return err
  }
  return nil
}

// UpdateMahasiswa
func UpdateMataKuliah(ctx context.Context, matkul models.MataKuliah, idMataKuliah string) error {
  db, err := config.MySQL()
  if err != nil {
    log.Fatal("Can't Connect to MySQL", err)
  }

  queryText := fmt.Sprintf("UPDATE %v set nama ='%v', updated_at = NOW() where id = %v",
    mata_kuliah_table,
    matkul.Nama,
    idMataKuliah,
  )

  _, err = db.ExecContext(ctx, queryText)
  if err != nil {
    return err
  }

  return nil
}

// Delete Movie
func DeleteMataKuliah(ctx context.Context, idMataKuliah string) error {
    db, err := config.MySQL()
    if err != nil {
        log.Fatal("Can't Connect to MySQL", err)
    }

    queryText := fmt.Sprintf("DELETE FROM %v where id = %s", mata_kuliah_table, idMataKuliah)

    s, err := db.ExecContext(ctx, queryText)

    if err != nil && err != sql.ErrNoRows {
        return err
    }

    check, err := s.RowsAffected()
    fmt.Println(check)
    if check == 0 {
        return errors.New("id tidak ada")
    }

    if err != nil {
        fmt.Println(err.Error())
    }

    return nil
}