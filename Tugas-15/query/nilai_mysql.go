package query

import (
	"context"
	"database/sql"
	"errors"
	"fmt"
	"log"
	"time"
	"tugas15/config"
	"tugas15/models"
)
const (
	nilai_table = "nilai"
)

//GetAll Nilai Mahasiswa
func GetAllNilai(ctx context.Context)([]models.Nilai, error) {
	var Nilai []models.Nilai
	db, err := config.MySQL()

	// config db check
	if err != nil {
		log.Fatal("Can't Connect to MySQL", err)
	}

	// query
	queryText := fmt.Sprintf("SELECT * FROM %v Order By created_at DESC", nilai_table)
	rowQuery, err := db.QueryContext(ctx, queryText)

	if err != nil {
		log.Fatal(err)
	}

	for rowQuery.Next() {
    var nilai models.Nilai
    var createdAt, updatedAt string
    if err = rowQuery.Scan(&nilai.ID,
		&nilai.IndeksNilai,
		&nilai.Skor,
		&createdAt,
		&updatedAt,
		&nilai.Mahasiswa,
		&nilai.MataKuliah); err != nil {
			return nil, err
    }

    //  Change format string to datetime for created_at and updated_at
    nilai.CreatedAt, err = time.Parse(layoutDateTime, createdAt)

    if err != nil {
      log.Fatal(err)
    }

    nilai.UpdatedAt, err = time.Parse(layoutDateTime, updatedAt)

    if err != nil {
      log.Fatal(err)
    }

    Nilai = append(Nilai, nilai)
  }
  return Nilai, nil
}

// Insert Nilai Mahasiswa
func InsertNilai(ctx context.Context, nilai models.Nilai) error {
  db, err := config.MySQL()

  // config db check
  if err != nil {
    log.Fatal("Can't Connect to MySQL", err)
  }
  
  // query
  queryText := fmt.Sprintf("INSERT INTO %v (skor, indeks, created_at, updated_at, mahasiswa_id, mata_kuliah_id) values(%v,'%v', NOW(), NOW(),%v,%v)", nilai_table,
    nilai.Skor,
    nilai.IndeksNilai,
    nilai.Mahasiswa,
    nilai.MataKuliah)
  _, err = db.ExecContext(ctx, queryText)

  if err != nil {
    return err
  }
  return nil
}

// Update Nilai Mahasiswa
func UpdateNilai(ctx context.Context, nilai models.Nilai, idNilai string) error {
  db, err := config.MySQL()
  if err != nil {
    log.Fatal("Can't Connect to MySQL", err)
  }

  queryText := fmt.Sprintf("UPDATE %v set skor = %v, indeks ='%v', updated_at = NOW() where id = %v",
    nilai_table,
    nilai.Skor,
    nilai.IndeksNilai,
    idNilai,
  )

  _, err = db.ExecContext(ctx, queryText)
  if err != nil {
    return err
  }

  return nil
}

// Delete Nilai Mahasiswa
func DeleteNilai(ctx context.Context, idNilai string) error {
    db, err := config.MySQL()
    if err != nil {
        log.Fatal("Can't Connect to MySQL", err)
    }

    queryText := fmt.Sprintf("DELETE FROM %v where id = %v", nilai_table, idNilai)

    s, err := db.ExecContext(ctx, queryText)

    if err != nil && err != sql.ErrNoRows {
        return err
    }

    check, err := s.RowsAffected()
    fmt.Println(check)
    if check == 0 {
        return errors.New("id tidak ada")
    }

    if err != nil {
        fmt.Println(err.Error())
    }

    return nil
}