package query

import (
	"context"
	"database/sql"
	"errors"
	"fmt"
	"log"
	"time"
	"tugas15/config"
	"tugas15/models"
)
const (
	mahasiswa_table = "mahasiswa"
	layoutDateTime  = "2006-01-02 15:04:05"
)

//GetAll
func GetAllMahasiswa(ctx context.Context)([]models.Mahasiswa, error) {
	var Mahasiswa []models.Mahasiswa
	db, err := config.MySQL()

	// config db check
	if err != nil {
		log.Fatal("Can't Connect to MySQL", err)
	}

	// query
	queryText := fmt.Sprintf("SELECT * FROM %v Order By created_at DESC", mahasiswa_table)
	rowQuery, err := db.QueryContext(ctx, queryText)

	if err != nil {
		log.Fatal(err)
	}

	for rowQuery.Next() {
    var mahasiswa models.Mahasiswa
    var createdAt, updatedAt string
    if err = rowQuery.Scan(&mahasiswa.ID,
      &mahasiswa.Nama,
      &createdAt,
      &updatedAt); err != nil {
      return nil, err
    }

    //  Change format string to datetime for created_at and updated_at
    mahasiswa.CreatedAt, err = time.Parse(layoutDateTime, createdAt)

    if err != nil {
      log.Fatal(err)
    }

    mahasiswa.UpdatedAt, err = time.Parse(layoutDateTime, updatedAt)

    if err != nil {
      log.Fatal(err)
    }

    Mahasiswa = append(Mahasiswa, mahasiswa)
  }
  return Mahasiswa, nil
}

// Insert Mahasiswa
func InsertMahasiswa(ctx context.Context, mahasiswa models.Mahasiswa) error {
  db, err := config.MySQL()

  // config db check
  if err != nil {
    log.Fatal("Can't Connect to MySQL", err)
  }
  
  // query
  queryText := fmt.Sprintf("INSERT INTO %v (nama, created_at, updated_at) values('%v', NOW(), NOW())", mahasiswa_table,
    mahasiswa.Nama)
  _, err = db.ExecContext(ctx, queryText)

  if err != nil {
    return err
  }
  return nil
}

// UpdateMahasiswa
func UpdateMahasiswa(ctx context.Context, mahasiswa models.Mahasiswa, idMahasiswa string) error {
  db, err := config.MySQL()
  if err != nil {
    log.Fatal("Can't Connect to MySQL", err)
  }

  queryText := fmt.Sprintf("UPDATE %v set nama ='%v', updated_at = NOW() where id = %v",
    mahasiswa_table,
    mahasiswa.Nama,
    idMahasiswa,
  )

  _, err = db.ExecContext(ctx, queryText)
  if err != nil {
    return err
  }

  return nil
}

// Delete Movie
func DeleteMahasiswa(ctx context.Context, idMahasiswa string) error {
    db, err := config.MySQL()
    if err != nil {
        log.Fatal("Can't Connect to MySQL", err)
    }

    queryText := fmt.Sprintf("DELETE FROM %v where id = %s", mahasiswa_table, idMahasiswa)

    s, err := db.ExecContext(ctx, queryText)

    if err != nil && err != sql.ErrNoRows {
        return err
    }

    check, err := s.RowsAffected()
    fmt.Println(check)
    if check == 0 {
        return errors.New("id tidak ada")
    }

    if err != nil {
        fmt.Println(err.Error())
    }

    return nil
}