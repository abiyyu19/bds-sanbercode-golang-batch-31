create database db_mahasiswa;

use db_mahasiswa;

CREATE TABLE mahasiswa (
  `id` INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `nama` VARCHAR(255) NOT NULL,
  `matkul` VARCHAR(255) NOT NULL,
  `nilai` INT NOT NULL,
  `indeks` VARCHAR(45) NOT NULL,
  `created_at` DATETIME NOT NULL,
  `updated_at` DATETIME NOT NULL
  );

desc mahasiswa;

alter table mahasiswa
drop column matkul,
drop column nilai,
drop column indeks;

create table nilai (
	id int auto_increment primary key,
    indeks varchar(255) not null,
    skor int not null,
    created_at datetime not null,
    updated_at datetime not null,
    mahasiswa_id int not null,
    mata_kuliah_id int not null
);

desc nilai;

create table mata_kuliah (
	id int auto_increment primary key,
    nama varchar(255) not null,
    created_at datetime not null,
    updated_at datetime not null
);

desc mata_kuliah;

alter table nilai
add foreign key (mata_kuliah_id) references mata_kuliah(id);

select * from mahasiswa;
select * from nilai;
select * from mata_kuliah;
