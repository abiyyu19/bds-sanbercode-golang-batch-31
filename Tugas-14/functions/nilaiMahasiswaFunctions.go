package functions

import (
	"context"
	"encoding/json"
	"fmt"
	"mahasiswa/models"
	"mahasiswa/query"
	"mahasiswa/utils"
	"net/http"

	"github.com/julienschmidt/httprouter"
)

// Function Indeks Nilai
func getIndeksNilai(nilai uint) string {
	switch {
	case nilai >= 80:
		return "A"
	case nilai >= 70 && nilai < 80:
		return "B"
	case nilai >= 60 && nilai < 70:
		return "C"
	case nilai >= 50 && nilai < 60:
		return "D"
	default:
		return "E"
	}
}

// Get Nilai Mahasiswa
func GetNilaiMahasiswa(rw http.ResponseWriter, _ *http.Request, _ httprouter.Params) {
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()
	nilaiMahasiswa, err := query.GetAllNilai(ctx)

  	if err != nil {
    	fmt.Println(err)
  	}

  	utils.ResponseJSON(rw, nilaiMahasiswa, http.StatusOK)
}

// Post Nilai Mahasiswa
func PostNilaiMahasiswa(rw http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	// pengecekan inputannya JSON atau bukan
	if r.Header.Get("Content-Type") != "application/json" {
		http.Error(rw, "Gunakan content type application / json", http.StatusBadRequest)
		return
	}
	
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()
	var nilaiMahasiswa models.NilaiMahasiswa
	if err := json.NewDecoder(r.Body).Decode(&nilaiMahasiswa); err != nil {
		utils.ResponseJSON(rw, err, http.StatusBadRequest)
		return
	}

	if nilaiMahasiswa.Nilai > 100 {
		http.Error(rw, "Nilai tidak boleh diinput lebih dari 100", http.StatusBadRequest)
		return
	}

	nilaiMahasiswa.IndeksNilai = getIndeksNilai(nilaiMahasiswa.Nilai)
	
	if err := query.InsertNilai(ctx, nilaiMahasiswa); err != nil {
		utils.ResponseJSON(rw, err, http.StatusInternalServerError)
		return
	}
	
	res := map[string]string{
		"status": "Succesfully",
	}
	
	utils.ResponseJSON(rw, res, http.StatusCreated)
}

// Update Nilai Mahasiswa
func UpdateNilaiMahasiswa(rw http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	if r.Header.Get("Content-Type") != "application/json" {
		http.Error(rw, "Gunakan content type application / json", http.StatusBadRequest)
		return
	}
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()
	var nilaiMahasiswa models.NilaiMahasiswa
	
	if err := json.NewDecoder(r.Body).Decode(&nilaiMahasiswa); err != nil {
		utils.ResponseJSON(rw, err, http.StatusBadRequest)
		return
	}
	
	var idNilaiMhs = ps.ByName("id")
	
	if nilaiMahasiswa.Nilai > 100 {
		http.Error(rw, "Nilai tidak boleh diinput lebih dari 100", http.StatusBadRequest)
		return
	}
	
	nilaiMahasiswa.IndeksNilai = getIndeksNilai(nilaiMahasiswa.Nilai)
	
	if err := query.UpdateNilai(ctx, nilaiMahasiswa, idNilaiMhs); err != nil {
		utils.ResponseJSON(rw, err, http.StatusInternalServerError)
		return
	}
	
	res := map[string]string{
		"status": "Succesfully",
	}

	utils.ResponseJSON(rw, res, http.StatusCreated)
}

// Delete Nilai Mahasiswa
func DeleteNilaiMahasiswa(rw http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()
	var idNilaiMhs = ps.ByName("id")

	if err := query.DeleteNilai(ctx, idNilaiMhs); err != nil {
		kesalahan := map[string]string{
			"error": fmt.Sprintf("%v", err),
		}
		utils.ResponseJSON(rw, kesalahan, http.StatusInternalServerError)
		return
	}
	
	res := map[string]string{
		"status": "Succesfully",
	}
	utils.ResponseJSON(rw, res, http.StatusOK)
}