package query

import (
	"context"
	"database/sql"
	"errors"
	"fmt"
	"log"
	"mahasiswa/config"
	"mahasiswa/models"
	"time"
)
const (
	table          = "mahasiswa1"
	layoutDateTime = "2006-01-02 15:04:05"
)

//GetAll
func GetAllNilai(ctx context.Context)([]models.NilaiMahasiswa, error) {
	var nilaiMahasiswa []models.NilaiMahasiswa
	db, err := config.MySQL()

	// config db check
	if err != nil {
		log.Fatal("Can't Connect to MySQL", err)
	}

	// query
	queryText := fmt.Sprintf("SELECT * FROM %v Order By created_at DESC", table)
	rowQuery, err := db.QueryContext(ctx, queryText)

	if err != nil {
		log.Fatal(err)
	}

	for rowQuery.Next() {
    var nilai models.NilaiMahasiswa
    var createdAt, updatedAt string
    if err = rowQuery.Scan(&nilai.ID,
      &nilai.Nama,
      &nilai.MataKuliah,
      &nilai.Nilai,
      &nilai.IndeksNilai,
      &createdAt,
      &updatedAt); err != nil {
      return nil, err
    }

    //  Change format string to datetime for created_at and updated_at
    nilai.CreatedAt, err = time.Parse(layoutDateTime, createdAt)

    if err != nil {
      log.Fatal(err)
    }

    nilai.UpdatedAt, err = time.Parse(layoutDateTime, updatedAt)

    if err != nil {
      log.Fatal(err)
    }

    nilaiMahasiswa = append(nilaiMahasiswa, nilai)
  }
  return nilaiMahasiswa, nil
}

// Insert Nilai Mahasiswa
func InsertNilai(ctx context.Context, nilai models.NilaiMahasiswa) error {
  db, err := config.MySQL()

  // config db check
  if err != nil {
    log.Fatal("Can't Connect to MySQL", err)
  }
  
  // query
  queryText := fmt.Sprintf("INSERT INTO %v (nama, matkul, nilai, indeks, created_at, updated_at) values('%v','%v',%v,'%v', NOW(), NOW())", table,
    nilai.Nama,
    nilai.MataKuliah,
    nilai.Nilai,
    nilai.IndeksNilai)
  _, err = db.ExecContext(ctx, queryText)

  if err != nil {
    return err
  }
  return nil
}

// Update Nilai Mahasiswa
func UpdateNilai(ctx context.Context, nilai models.NilaiMahasiswa, idNilai string) error {
  db, err := config.MySQL()
  if err != nil {
    log.Fatal("Can't Connect to MySQL", err)
  }

  queryText := fmt.Sprintf("UPDATE %v set nama ='%v', matkul = '%v', nilai = %v, indeks = '%v', updated_at = NOW() where id = %v",
    table,
    nilai.Nama,
    nilai.MataKuliah,
    nilai.Nilai,
    nilai.IndeksNilai,
    idNilai,
  )

  _, err = db.ExecContext(ctx, queryText)
  if err != nil {
    return err
  }

  return nil
}

// Delete Movie
func DeleteNilai(ctx context.Context, idNilai string) error {
    db, err := config.MySQL()
    if err != nil {
        log.Fatal("Can't Connect to MySQL", err)
    }

    queryText := fmt.Sprintf("DELETE FROM %v where id = %s", table, idNilai)

    s, err := db.ExecContext(ctx, queryText)

    if err != nil && err != sql.ErrNoRows {
        return err
    }

    check, err := s.RowsAffected()
    fmt.Println(check)
    if check == 0 {
        return errors.New("id tidak ada")
    }

    if err != nil {
        fmt.Println(err.Error())
    }

    return nil
}