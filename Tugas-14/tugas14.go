package main

import (
	"fmt"
	"log"
	"mahasiswa/functions"
	"net/http"

	"github.com/julienschmidt/httprouter"
) 

func main() {
  router := httprouter.New()
  router.GET("/nilai-mahasiswa", functions.GetNilaiMahasiswa)
  router.POST("/nilai-mahasiswa/create", functions.PostNilaiMahasiswa)
  router.PUT("/nilai-mahasiswa/:id/update", functions.UpdateNilaiMahasiswa)
  router.DELETE("/nilai-mahasiswa/:id/delete", functions.DeleteNilaiMahasiswa)

  fmt.Println("Server Running at Port 8080")
  log.Fatal(http.ListenAndServe("localhost:8080", router))
}