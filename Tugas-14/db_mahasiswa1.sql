create database db_mahasiswa1;

use db_mahasiswa1;

CREATE TABLE mahasiswa1 (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `nama` VARCHAR(255) NOT NULL,
  `matkul` VARCHAR(255) NOT NULL,
  `nilai` INT UNSIGNED NOT NULL,
  `indeks` VARCHAR(45) NOT NULL,
  `created_at` DATETIME NOT NULL,
  `updated_at` DATETIME NOT NULL
  );

desc mahasiswa1;

select * from mahasiswa1;