package main

import (
	"errors"
	"flag"
	"fmt"
	"math"
	"sort"
	"strconv"
	"time"
)

// SOAL 1
func tampil(kalimat string, tahun int) {
	fmt.Println(`"NO 1 -->"`, kalimat, tahun)
}

// SOAL 2
func eror() {
	pesan := recover()
	fmt.Println(pesan)
}
func runPanic(err bool) {
	defer eror()
	if err {
		panic("ERROR")
	}
}

func kelilingSegitigaSamaSisi(sisi int, nilai bool) (string, error) {
	keliling := sisi*3
	
	if sisi == 0 {
		if nilai == true {
			return "ERROR", errors.New("Maaf anda belum menginput sisi dari segitiga sama sisi")
		} else {
			runPanic(true)
			return "ERROR", errors.New("Maaf anda belum menginput sisi dari segitiga sama sisi")
		}
	} else {
		if nilai == true {
			return "keliling segitiga sama sisinya dengan sisi " + strconv.Itoa(sisi) + " cm adalah " + strconv.Itoa(keliling) + " cm", nil
		} else {
			return strconv.Itoa(sisi), nil
		}
	}

	// cara trainer
	// switch{
	// case sisi > 0 && nilai:
	// 	return "keliling segitiga sama sisinya dengan sisi " + strconv.Itoa(sisi) + " cm adalah " + strconv.Itoa(sisi*3) + " cm", nil
	// case sisi > 0 && !nilai:
	// 	return strconv.Itoa(sisi*3), nil
	// case sisi <= 0 && nilai:
	// 	return "", errors.New("Maaf anda belum menginput sisi dari segitiga sama sisi")
	// default:
	// 	runPanic(true)
	// 	return "", errors.New("Maaf anda belum menginput sisi dari segitiga sama sisi")
	// }
}

// SOAL 3
func tambahAngka(nilai int, angka *int) {
	*angka += nilai
}
func cetakAngka(angka *int) {
	fmt.Println(`"NO 3 -->"`, *angka)
}

// SOAL 4
func tambahPhone(phones *[]string, merk ...string) {
	*phones = append(*phones, merk...)

	sort.Strings(*phones)

	for i, item := range *phones {
		fmt.Printf("%d. %s\n", i+1, item)
		time.Sleep(time.Second)
	}

}

// SOAL 5
func luasdankelilingLingkaran(r float64) (luas float64, keliling float64) {
	const phi = 3.14
	luas = math.Round(phi * r * r)
	keliling = math.Round(phi * r * 2)
	return luas, keliling
	
}

func main() {
	// deklarasi variabel angka ini simpan di baris pertama func main
	angka := 1

	// SOAL 1
	defer tampil("Golang Backend Development", 2021)
	
	// SOAL 2
	fmt.Println("------Soal 2------")
		
	fmt.Println(kelilingSegitigaSamaSisi(4, true))

	fmt.Println(kelilingSegitigaSamaSisi(8, false))

	fmt.Println(kelilingSegitigaSamaSisi(0, true))

	fmt.Println(kelilingSegitigaSamaSisi(0, false))

	// SOAL 3
	defer cetakAngka(&angka)

	tambahAngka(7, &angka)

	tambahAngka(6, &angka)

	tambahAngka(-1, &angka)

	tambahAngka(9, &angka)

	// SOAL 4
	fmt.Println("------Soal 4------")
	var phones = []string{}

	tambahPhone(&phones, "Xiaomi", "Asus", "IPhone", "Samsung", "Oppo", "Realme", "Vivo")

	// SOAL 5
	fmt.Println("------Soal 5------")
	fmt.Println("--jari-jari 7--")
	fmt.Println(luasdankelilingLingkaran(7))
	fmt.Println("--jari-jari 10--")
	fmt.Println(luasdankelilingLingkaran(10))
	fmt.Println("--jari-jari 15--")
	fmt.Println(luasdankelilingLingkaran(15))

	// SOAL 6
	fmt.Println("------Soal 6------")
	var panjang = flag.Int("panjang", 0, "masukkan panjang")
	var lebar = flag.Int("lebar", 0, "masukkan lebar")

	flag.Parse()
	fmt.Println("Luas Persegi Panjang :", *panjang * *lebar)
	fmt.Println("Keliling Persegi Panjang :", 2**panjang + 2**lebar)
	fmt.Println()
}