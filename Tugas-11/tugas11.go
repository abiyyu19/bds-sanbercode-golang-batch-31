package main

import (
	"fmt"
	"math"
	"sort"
	"strconv"
	"sync"
	"time"
)

// SOAL 1
func phone(phones []string, wg *sync.WaitGroup) {
	for i, items := range phones {
		fmt.Print(i+1, ". ")
		fmt.Println(items)
		time.Sleep(time.Second)
	}
	wg.Done()
} 

// SOAL 2
func getMovies(mv chan string, movies ...string) {
	for i, item := range movies {
		mv <- strconv.Itoa(i+1) + ". " + item
	}
	close(mv)
}

// SOAL 3
func luasLingkaran(luas chan float64, r float64) {
	const phi = 3.14
	luas <- math.Round(phi * r * r)
	
}
func kelilingLingkaran(keliling chan float64, r float64) {
	const phi = 3.14
	keliling <- math.Round(phi * r * 2)
}
func volumeTabung(volume chan float64, r, t float64) {
	const phi = 3.14
	volume <- math.Round(phi * r * r * t)
}

// SOAL 4
func luasPersegiPanjang(luas chan int, p, l int) {
	luas <- p * l
	
}
func kelilingPersegiPanjang(keliling chan int, p, l int) {
	keliling <- 2*p + 2*l
}
func volumeBalok(volume chan int, p, l, t int) {
	volume <- p*l*t
}

func main() {
	// SOAL 1
	fmt.Println("------Soal 1------")
	var phones = []string{"Xiaomi", "Asus", "Iphone", "Samsung", "Oppo", "Realme", "Vivo"}
	sort.Strings(phones)

	var wg sync.WaitGroup

	wg.Add(1)
	go phone(phones, &wg)
	wg.Wait()
	
	// SOAL 2
	fmt.Println("\n------Soal 2------")
	
	var movies = []string{"Harry Potter", "LOTR", "SpiderMan", "Logan", "Avengers", "Insidious", "Toy Story"}

	moviesChannel := make(chan string)

	go getMovies(moviesChannel, movies...)

	fmt.Println("List Movies:")
	for value := range moviesChannel {
		fmt.Println(value)
	}
	// for {
	// 	_, ok := <- moviesChannel
    // 	if ok == false {
    //   		break
    // 	}
	// }

	// SOAL 3
	fmt.Println("\n------Soal 3------")
	luasLingkaran1 := make(chan float64)
	kelilingLingkaran1 := make(chan float64)
	volumeTabung1 := make(chan float64)

	go luasLingkaran(luasLingkaran1, 8)
	go luasLingkaran(luasLingkaran1, 14)
	go luasLingkaran(luasLingkaran1, 20)
	go kelilingLingkaran(kelilingLingkaran1, 8)
	go kelilingLingkaran(kelilingLingkaran1, 14)
	go kelilingLingkaran(kelilingLingkaran1, 20)
	go volumeTabung(volumeTabung1, 8, 10)
	go volumeTabung(volumeTabung1, 14, 10)
	go volumeTabung(volumeTabung1, 20, 10)

	fmt.Println("--Lingkaran dengan Jari-Jari 8--\nLuas:", <-luasLingkaran1, "\nKeliling:", <-kelilingLingkaran1)
	fmt.Println("\n--Lingkaran dengan Jari-Jari 14--\nLuas:", <-luasLingkaran1, "\nKeliling:", <-kelilingLingkaran1)
	fmt.Println("\n--Lingkaran dengan Jari-Jari 20--\nLuas:", <-luasLingkaran1, "\nKeliling:", <-kelilingLingkaran1)
	fmt.Println("\nVolume Tabung dengan Jari-Jari 8 dan Tinggi 10:", <-volumeTabung1)
	fmt.Println("Volume Tabung dengan Jari-Jari 14 dan Tinggi 10:", <-volumeTabung1)
	fmt.Println("Volume Tabung dengan Jari-Jari 20 dan Tinggi 10:", <-volumeTabung1)

	// SOAL 4
	fmt.Println("\n------Soal 4------")
	luasPersegiPanjang1 := make(chan int)
	kelilingPersegiPanjang1 := make(chan int)
	volumeBalok1 := make(chan int)

	go luasPersegiPanjang(luasPersegiPanjang1, 6, 8)
	go kelilingPersegiPanjang(kelilingPersegiPanjang1, 6, 8)
	go volumeBalok(volumeBalok1, 6, 8, 10)

	for i := 0; i < 3; i++ {
		select {
			case luas := <-luasPersegiPanjang1:
				fmt.Println("Luas Persegi Panjang\t :", luas)
			case keliling := <-kelilingPersegiPanjang1:
				fmt.Println("Keliling Persegi Panjang :", keliling)
			case volume := <-volumeBalok1:
				fmt.Println("Volume Balok\t\t :", volume)
		}
	}
}
