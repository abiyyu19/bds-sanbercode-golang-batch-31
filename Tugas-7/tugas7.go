package main

import (
	"fmt"
)

// SOAL 1
type buah struct {
	nama, warna string
	adaBijinya bool
	harga int
}

// SOAL 2
type segitiga struct{
  alas, tinggi int
}
func (s segitiga) luasSegitiga() {
	fmt.Println("Luas Segitiga :", s.alas*s.tinggi/2)
}
type persegi struct{
  sisi int
}
func (p persegi) luasPersegi() {
	fmt.Println("Luas Persegi :", p.sisi*p.sisi)
}
type persegiPanjang struct{
  panjang, lebar int
}
func (pp persegiPanjang) luasPersegiPanjang() {
	fmt.Println("Luas Persegi Panjang :", pp.panjang*pp.lebar)
}

// SOAL 3
type phone struct{
   name, brand string
   year int
   colors []string
}
func (hp phone) tambahWarna(colors *[]string, warna ...string) {
	hp.colors = append(hp.colors, warna...)
	fmt.Println("Nama\t:", hp.name)
	fmt.Println("Brand\t:", hp.brand)
	fmt.Println("Tahun\t:", hp.year)
	fmt.Println("Warna\t:", hp.colors)
}

// SOAL 4
type movie struct {
	title, genre string
	duration, year int
}
func tambahDataFilm(title string, duration int, genre string, year int, dataFilm *[]movie) {
	// var mv = movie{title, genre, duration, year}
	
	// *dataFilm = append(*dataFilm, mv)

	*dataFilm = append(*dataFilm, movie{title, genre, duration, year})
}
func main()  {
	// SOAL 1
	fmt.Println("------Soal 1------")
	// objek nanas
	var nanas buah
	nanas.nama = "Nanas"
	nanas.warna = "Kuning"
	nanas.adaBijinya = false
	nanas.harga = 9000

	// objek jeruk
	var jeruk = buah{"Jeruk", "Oranye", true, 8000}

	// objek semangka
	var semangka = buah{nama: "Semangka", harga: 10000, warna: "Hijau & Merah", adaBijinya: true}

	// objek pisang
	var pisang = buah{"Pisang", "Kuning", false, 5000}

	fmt.Println("Nama\t\t Warna\t\t AdaBijinya\t Harga")
	fmt.Println(nanas.nama,"\t\t",nanas.warna,"\t", nanas.adaBijinya,"\t\t",nanas.harga)
	fmt.Println(jeruk.nama,"\t\t",jeruk.warna,"\t", jeruk.adaBijinya,"\t\t",jeruk.harga)
	fmt.Println(semangka.nama,"\t",semangka.warna,"\t", semangka.adaBijinya,"\t\t",semangka.harga)
	fmt.Println(pisang.nama,"\t\t",pisang.warna,"\t", pisang.adaBijinya,"\t\t",pisang.harga)

	// SOAL 2
	fmt.Println("------Soal 2------")
	// Luas Segitiga
	var s = segitiga{6,8}
	s.luasSegitiga()
	
	// Luas Persegi
	var p = persegi{8}
	p.luasPersegi()

	// Luas Persegi
	var pp = persegiPanjang{6,8}
	pp.luasPersegiPanjang()

	// SOAL 3
	fmt.Println("------Soal 3------")
	// Ojbjek asus
	var asus = phone{name: "Asus Zenfone 5", brand: "ASUS",year: 2016}
	asus.tambahWarna(&asus.colors, "Merah", "Kuning", "Putih")
	
	fmt.Println()

	// Objek xiaomi
	var xiaomi = phone{name: "Xiaomi Mi 11x", brand: "XIAOMI",year: 2021}
	xiaomi.tambahWarna(&xiaomi.colors, "Hitam", "Biru", "Pink")

	// SOAL 4
	fmt.Println("------Soal 4------")
	var dataFilm = []movie{}

	tambahDataFilm("LOTR", 120, "action", 1999, &dataFilm)
	tambahDataFilm("avenger", 120, "action", 2019, &dataFilm)
	tambahDataFilm("spiderman", 120, "action", 2004, &dataFilm)
	tambahDataFilm("juon", 120, "horror", 2004, &dataFilm)

	// isi dengan jawaban anda untuk menampilkan data
	for index, item := range dataFilm {
		fmt.Print(index+1)
		fmt.Print(". ")
		fmt.Println("title :", item.title)
		fmt.Println("   duration :", item.duration/60, "jam")
		fmt.Println("   genre :", item.genre)
		fmt.Println("   year :", item.year)
		fmt.Println()
	}
}