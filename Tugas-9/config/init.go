package config

import (
	"fmt"
	"strconv"
)

var Prefix interface{}

var KumpulanAngkaPertama interface{}

var KumpulanAngkaKedua interface{}

// tulis jawaban anda disini
func init() {
	Prefix = "hasil penjumlahan dari "
	KumpulanAngkaPertama  = []int{6, 8}
	KumpulanAngkaKedua  = []int{12, 14}
	
	var kata = Prefix.(string)
	angka1 := 0
	batas := 0
	for _, item := range KumpulanAngkaPertama.([]int) {
		angka1 += item
		kata = kata + strconv.Itoa(item) + " + "
	}
	for _, item := range KumpulanAngkaKedua.([]int) {
		angka1 += item
		kata = kata + strconv.Itoa(item)
		batas++
		if batas < len(KumpulanAngkaKedua.([]int)) {
			kata += " + "
		}
	}
	fmt.Println(kata, "=", angka1)
}