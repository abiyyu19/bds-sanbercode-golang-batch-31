package helper

type SegitigaSamaSisi struct {
	Alas, Tinggi int
}

// Method luas dan keliling segitigaSamaSisi
func (s SegitigaSamaSisi) Luas() int {
	return s.Alas * s.Tinggi / 2
}
func (s SegitigaSamaSisi) Keliling() int {
	return s.Alas * 3
}

type PersegiPanjang struct {
	Panjang, Lebar int
}

// Method luas dan keliling persegiPanjang
func (p PersegiPanjang) Luas() int {
	return p.Panjang * p.Lebar
}
func (p PersegiPanjang) Keliling() int {
	return 2*p.Panjang + 2*p.Lebar
}

type Tabung struct {
	JariJari, Tinggi float64
}

// Method volume dan luasPermukaan tabung
func (t Tabung) Volume() float64 {
	const phi = 3.14
	return phi * t.JariJari * t.JariJari * t.Tinggi
}
func (t Tabung) LuasPermukaan() float64 {
	const phi = 3.14
	return 2 * phi * t.JariJari * (t.JariJari + t.Tinggi)
}

type Balok struct {
	Panjang, Lebar, Tinggi int
}

// Method volume dan luasPermukaan balok
func (b Balok) Volume() float64 {
	return float64(b.Panjang) * float64(b.Lebar) * float64(b.Tinggi)
}
func (b Balok) LuasPermukaan() float64 {
	return (2 * float64(b.Panjang) * float64(b.Lebar)) + (2 * float64(b.Panjang) * float64(b.Tinggi)) + (2 * float64(b.Lebar) * float64(b.Tinggi))
}

type HitungBangunDatar interface {
	Luas() int
	Keliling() int
}

type HitungBangunRuang interface {
	Volume() float64
	LuasPermukaan() float64
}