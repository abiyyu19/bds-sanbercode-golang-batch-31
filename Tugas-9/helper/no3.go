package helper

import "strconv"

func LuasPersegi(sisi int, nilai bool) interface{} {
	luas := sisi * sisi

	if sisi == 0 {
		if nilai == true {
			return "Maaf anda belum menginput sisi dari persegi"
		} else {
			return nil
		}
	} else {
		if nilai == true {
			return "luas persegi dengan sisi " + strconv.Itoa(sisi) + " cm adalah " + strconv.Itoa(luas) + " cm"
		} else {
			return luas
		}
	}
}