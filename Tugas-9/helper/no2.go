package helper

import "fmt"

// SOAL 2
type Phone struct{
   Name, Brand string
   Year int
   Colors []string
}
type TampilkanData interface {
	TambahWarna()
	TampilData()
}
func (hp *Phone) TambahWarna(Colors *[]string, Warna ...string) {
	hp.Colors = append(hp.Colors, Warna...)
}
func (hp Phone) TampilData() {
	fmt.Println("name :", hp.Name)
	fmt.Println("brand :", hp.Brand)
	fmt.Println("year :", hp.Year)
	var warna string
	batas := 0
	for _, item := range hp.Colors {
		warna = warna + item
		batas++
		if batas < len(hp.Colors) {
			warna += ", "
		}
	}
	fmt.Println("colors :", warna)
}