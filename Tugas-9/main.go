package main

import (
	_ "Tugas-9/config"
	"Tugas-9/helper"
	"fmt"
)

func main() {

	// SOAL 4
	fmt.Println("------Soal 4------")
	fmt.Println("Soal 4 ada di atas ini")
	fmt.Println()
	
	// SOAL 1
	fmt.Println("------Soal 1------")
	// Segitiga
	var segitiga helper.HitungBangunDatar = helper.SegitigaSamaSisi{6,8}
	fmt.Println("Luas Segitiga :", segitiga.Luas())
	fmt.Println("Keliling Segitiga :", segitiga.Keliling())

	// PersegiPanjang
	var persegiPanjang helper.HitungBangunDatar = helper.PersegiPanjang{6,8}
	fmt.Println("\nLuas Persegi Panjang :", persegiPanjang.Luas())
	fmt.Println("Keliling Persegi Panjang :", persegiPanjang.Keliling())

	// Tabung
	var tabung helper.HitungBangunRuang = helper.Tabung{7,8}
	fmt.Println("\nVolume Tabung :", tabung.Volume())
	fmt.Println("Luas Permukaan Tabung :", tabung.LuasPermukaan())
	
	// Balok
	var balok helper.HitungBangunRuang = helper.Balok{7,8,9}
	fmt.Println("\nVolume Balok :", balok.Volume())
	fmt.Println("Luas Permukaan Balok :", balok.LuasPermukaan())

	// SOAL 2
	fmt.Println("------Soal 2------")
	var samsung = helper.Phone{Name: "Samsung Galaxy Note 20", Brand: "Samsung Galaxy Note 20", Year: 2020}
	samsung.TambahWarna(&samsung.Colors, "Mystic Bronze", "Mystic White", "Mystic Black")
	samsung.TampilData()

	// SOAL 3
	fmt.Println("------Soal 3------")
	fmt.Println(helper.LuasPersegi(4, true))

	fmt.Println(helper.LuasPersegi(8, false))

	fmt.Println(helper.LuasPersegi(0, true))

	fmt.Println(helper.LuasPersegi(0, false))
}