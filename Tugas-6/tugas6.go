package main

import (
	"fmt"
)

// SOAL 1
func luasdankelilingLingkaran(luasLingkaran *float64, kelilingLingkaran *float64, r float64) {
	const phi = 3.14
	*luasLingkaran = phi * r * r
	*kelilingLingkaran = phi * r * 2
}

// SOAL 2
func introduce(sentence *string, nama string, gender string, pekerjaan string, usia string) {
	if gender == "laki-laki" {
		*sentence = "Pak " + nama + " adalah seorang " + pekerjaan + " yang berusia " + usia + " tahun"
	} else if gender == "perempuan" {
		*sentence = "Bu " + nama + " adalah seorang " + pekerjaan + " yang berusia " + usia + " tahun"
	}
}

// SOAL 3
func tambahBuah(buah *[]string) {
	*buah = append(*buah, "Jeruk", "Semangka", "Mangga", "Strawberry", "Durian", "Manggis", "Alpukat")

	for i, fruits := range *buah {
		fmt.Printf("%d. %s\n", i+1, fruits)
	}
}

// SOAL 4
func tambahDataFilm(judul string, durasi string, genre string, tahun string, dataFilm *[]map[string]string) {
	item := map[string]string {
		"genre": genre,
		"jam": durasi,
		"tahun": tahun,
		"title": judul,
	}
	
	*dataFilm = append(*dataFilm, item)
}

func main() {
	// SOAL 1
	fmt.Println("------Soal 1------")
	var luasLingkaran float64 
	var kelilingLingkaran float64

	luasdankelilingLingkaran(&luasLingkaran, &kelilingLingkaran, 7)

	fmt.Println(luasLingkaran)
	fmt.Println(kelilingLingkaran)

	// SOAL 2
	fmt.Println("------Soal 2------")
	var sentence string 

	introduce(&sentence, "John", "laki-laki", "penulis", "30")
	fmt.Println(sentence) // Menampilkan "Pak John adalah seorang penulis yang berusia 30 tahun"

	introduce(&sentence, "Sarah", "perempuan", "model", "28")
	fmt.Println(sentence) // Menampilkan "Bu Sarah adalah seorang model yang berusia 28 tahun"

	// SOAL 3
	fmt.Println("------Soal 3------")
	var buah = []string{}

	tambahBuah(&buah)

	// SOAL 4
	fmt.Println("------Soal 4------")
	var dataFilm = []map[string]string{}

	tambahDataFilm("LOTR", "2 jam", "action", "1999", &dataFilm)
	tambahDataFilm("avenger", "2 jam", "action", "2019", &dataFilm)
	tambahDataFilm("spiderman", "2 jam", "action", "2004", &dataFilm)
	tambahDataFilm("juon", "2 jam", "horror", "2004", &dataFilm)

	// for i, val := range dataFilm {
	// 	fmt.Printf("%d. title : %s\n   duration : %s\n   genre : %s\n   year : %s\n",i+1, val["title"], val["jam"], val["genre"], val["tahun"])
	// }
	for index, item := range dataFilm {
		fmt.Print(index+1)
		fmt.Print(". ")
		i := 1
		for key, value := range item {
			if i == 1 {
				fmt.Println(key, ":", value)
			} else {	
				fmt.Println("  ", key, ":", value)
			}
			i++
		}
		fmt.Println()
	}
}